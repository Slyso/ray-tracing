use crate::{HitRange, Hittable, Ray};
use crate::hit::Hit;
use crate::material::Material;

pub type World = Vec<Box<dyn Hittable>>;

impl Hittable for World {
    fn hit(&self, ray: &Ray, mut range: HitRange) -> Option<Hit> {
        let mut closest_hit = None;

        for entity in self {
            if let Some(hit) = entity.hit(ray, range) {
                range = HitRange { min: range.min, max: hit.t() };
                closest_hit = Some(hit);
            }
        }

        closest_hit
    }

    fn material(&self) -> &dyn Material {
        panic!("The world has no material")
    }
}