use std::fmt::{Display, Formatter};

use crate::image_writer::MAX_COLOUR_VALUE;
use crate::vec3::Vec3;
use std::ops::{Mul, Add};

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct Colour {
    vector: Vec3,
    gamma: Option<f64>,
}

impl Colour {
    pub fn new(red: f64, green: f64, blue: f64) -> Colour {
        assert!(red >= 0.0 && red <= 1.0, "invalid red value {}", red);
        assert!(green >= 0.0 && green <= 1.0, "invalid green value {}", green);
        assert!(blue >= 0.0 && blue <= 1.0, "invalid blue value {}", blue);

        Colour {
            vector: Vec3::new(red, green, blue),
            gamma: None,
        }
    }

    pub fn with_gamma_correction(&mut self, gamma: Option<f64>) -> Colour {
        self.gamma = gamma;
        *self
    }

    pub fn average(colours: Vec<Colour>) -> Colour {
        let x = colours.iter().map(|c| c.vector.x()).sum::<f64>() / colours.len() as f64;
        let y = colours.iter().map(|c| c.vector.y()).sum::<f64>() / colours.len() as f64;
        let z = colours.iter().map(|c| c.vector.z()).sum::<f64>() / colours.len() as f64;
        Colour::new(x, y, z)
    }
}

impl Add<Colour> for Colour {
    type Output = Colour;
    fn add(self, rhs: Colour) -> Colour { Colour::from(self.vector + rhs.vector) }
}

impl Mul<f64> for Colour {
    type Output = Colour;
    fn mul(self, rhs: f64) -> Colour { Colour::from(self.vector * rhs) }
}

impl Mul<Colour> for Colour {
    type Output = Colour;
    fn mul(self, rhs: Colour) -> Colour { Colour::from(self.vector * rhs.vector) }
}

impl From<Vec3> for Colour {
    fn from(v: Vec3) -> Self {
        Colour::new(v.x(), v.y(), v.z())
    }
}

impl Display for Colour {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut vector = self.vector.clone();

        if let Some(gamma) = self.gamma {
            vector = Vec3::new(
                vector.x().powf(1.0 / gamma),
                vector.y().powf(1.0 / gamma),
                vector.z().powf(1.0 / gamma),
            )
        }

        let v = vector * (MAX_COLOUR_VALUE as f64 + 0.999);

        write!(f, "{} {} {}",
               v.x() as u32,
               v.y() as u32,
               v.z() as u32,
        )
    }
}
