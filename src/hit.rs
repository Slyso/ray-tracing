use crate::{Point3, Ray, Vec3};
use crate::material::Material;

pub trait Hittable {
    fn hit(&self, ray: &Ray, range: HitRange) -> Option<Hit>;

    fn material(&self) -> &dyn Material;
}

/// The result of a `Ray` hitting `object` at `Point3` `ray.at(t)` creating a `normal`
pub struct Hit<'a> {
    t: f64,
    point: Point3,
    normal: Vec3,
    normal_side: Side,
    object: &'a dyn Hittable,
}

impl Hit<'_> {
    pub fn new<'a, 'b>(ray: &'a Ray, t: f64, point: Point3, outward_normal: &Vec3, object: &'b dyn Hittable) -> Hit<'b> {
        let (normal_side, normal) = Hit::get_face_normal(ray, outward_normal);

        Hit {
            t,
            point,
            normal,
            normal_side,
            object,
        }
    }

    pub fn t(&self) -> f64 { self.t }
    pub fn point(&self) -> Point3 { self.point }
    pub fn normal(&self) -> Vec3 { self.normal }
    pub fn object(&self) -> &dyn Hittable { self.object }

    fn get_face_normal(ray: &Ray, outward_normal: &Vec3) -> (Side, Vec3) {
        if ray.direction.dot(*outward_normal) < 0.0 {
            (Side::Front, *outward_normal)
        } else {
            (Side::Back, -*outward_normal)
        }
    }
}

#[derive(Eq, PartialEq, Debug)]
pub enum Side {
    Front,
    Back,
}

#[derive(PartialEq, Copy, Clone)]
pub struct HitRange {
    pub min: f64,
    pub max: f64,
}

impl HitRange {
    pub fn in_range(&self, value: f64) -> bool {
        self.min <= value && value <= self.max
    }
}