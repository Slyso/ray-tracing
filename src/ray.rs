use crate::vec3::{Point3, Vec3};

pub struct Ray {
    pub origin: Point3,
    pub direction: Vec3,
}

impl Ray {
    pub fn new(origin: Point3, direction: Vec3) -> Ray {
        Ray { origin, direction }
    }

    pub fn at(&self, t: f64) -> Point3 {
        self.origin + self.direction * t
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn at() {
        let ray = Ray::new((1, 3, -1).into(), (1, -2, 4).into());

        assert_eq!(ray.at(-1.0), (0, 5, -5).into());
        assert_eq!(ray.at(0.0), (1, 3, -1).into());
        assert_eq!(ray.at(1.0), (2, 1, 3).into());
        assert_eq!(ray.at(2.0), (3, -1, 7).into());
    }
}