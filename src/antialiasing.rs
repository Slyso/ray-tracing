use std::ops::Range;

use crate::Colour;

pub struct Antialiasing {
    raster_size: u8,
}

impl Antialiasing {
    pub fn new(raster_size: u8) -> Antialiasing {
        Antialiasing { raster_size }
    }

    /// Call back `f` `raster_size * raster_size` times with interpolated values of `x` and `y`
    pub fn render<F>(&self, x: Range<f64>, y: Range<f64>, mut f: F) -> Colour
        where F: FnMut(f64, f64) -> Colour {
        let width = (x.end - x.start) / self.raster_size as f64;
        let height = (y.end - y.start) / self.raster_size as f64;

        let x_offset = x.start + width / 2.0;
        let y_offset = y.start + height / 2.0;

        let mut colours: Vec<Colour> = vec![];

        for x in 0..self.raster_size {
            for y in 0..self.raster_size {
                colours.push(f(x_offset + width * x as f64, y_offset + height * y as f64));
            }
        }

        Colour::average(colours)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn antialiasing_single_point() {
        let antialiasing = Antialiasing::new(1);
        let expected_rendered_points = vec![(0.5, 0.5)];
        let mut rendered_points = vec![];

        antialiasing.render(0.0..1.0, 0.0..1.0, |a, b| {
            rendered_points.push((a, b));
            Colour::new(0.0, 0.0, 0.0)
        });

        assert_eq!(rendered_points, expected_rendered_points);
    }

    #[test]
    fn antialiasing_two_by_two() {
        let antialiasing = Antialiasing::new(2);
        let expected_rendered_points = vec![(0.25, 0.25), (0.25, 0.75), (0.75, 0.25), (0.75, 0.75)];
        let mut rendered_points = vec![];

        antialiasing.render(0.0..1.0, 0.0..1.0, |a, b| {
            rendered_points.push((a, b));
            Colour::new(0.0, 0.0, 0.0)
        });

        assert_eq!(rendered_points, expected_rendered_points);
    }
}
