use std::rc::Rc;
use rand::Rng;

use crate::antialiasing::Antialiasing;
use crate::camera::Camera;
use crate::colour::Colour;
use crate::hit::{HitRange, Hittable};
use crate::image_writer::ImageWriter;
use crate::material::{Diffuse, Material, Metal};
use crate::ray::Ray;
use crate::sphere::Sphere;
use crate::vec3::{Point3, Vec3};
use crate::world::World;

mod antialiasing;
mod camera;
mod colour;
mod hit;
mod image_writer;
mod material;
mod ray;
mod sphere;
mod vec3;
mod world;

const ASPECT_RATIO: f64 = 16.0 / 9.0;
const IMAGE_HEIGHT: u32 = 400;

const MAX_RAY_DEPTH: u8 = 10;
const ANTIALIASING_RASTER_SIZE: u8 = 20;

fn main() {
    let world = get_world();
    let camera = Camera::new(ASPECT_RATIO);
    render_image(world, camera);
}

fn get_world() -> World {
    let mut world = World::new();

    for _ in 0..7 {
        let metal: Rc<dyn Material> = if rand::thread_rng().gen_bool(0.5) {
            Rc::new(Metal { albedo: Colour::from(Vec3::new_random(0.1..0.9, 0.1..0.9, 0.1..0.9)) })
        } else {
            Rc::new(Diffuse { albedo: Colour::from(Vec3::new_random(0.1..0.9, 0.1..0.9, 0.1..0.9)) })
        };

        world.push(Box::new(Sphere { center: Vec3::new_random(-2.0..2.0, -0.2..0.4, -1.5..-0.5), radius: 0.3, material: metal.clone() }));
    }

    let diffuse_green = Rc::new(Diffuse { albedo: Colour::new(0.0, 0.6, 0.0) });
    world.push(Box::new(Sphere { center: (0, -100.5, -1).into(), radius: 100.0, material: diffuse_green }));

    world
}

fn render_image(world: World, camera: Camera) {
    let image_width = (IMAGE_HEIGHT as f64 * ASPECT_RATIO) as u32;
    let writer = ImageWriter::new(image_width, IMAGE_HEIGHT);

    let antialiasing = Antialiasing::new(ANTIALIASING_RASTER_SIZE);

    writer.write_image(|x, y| {
        antialiasing.render(x, y, |x, y| {
            ray_colour(&camera.get_ray(x, y), &world, MAX_RAY_DEPTH)
        }).with_gamma_correction(Some(2.0))
    }).unwrap();
}

fn ray_colour(incoming_ray: &Ray, world: &World, depth: u8) -> Colour {
    if depth == 0 {
        return Colour::new(0.0, 0.0, 0.0);
    }

    if let Some(hit) = world.hit(incoming_ray, HitRange { min: 0.01, max: f64::INFINITY }) {
        let (reflected_ray, albedo) = hit.object().material().reflect(incoming_ray, &hit);
        ray_colour(&reflected_ray, world, depth - 1) * albedo
    } else {
        background_colour(incoming_ray)
    }
}

fn background_colour(ray: &Ray) -> Colour {
    let unit_direction = ray.direction.normalize();

    let white = Colour::new(1.0, 1.0, 1.0);
    let blue = Colour::new(0.3, 0.7, 1.0);

    let t = 0.5 * (unit_direction.y() + 1.0);
    white * (1.0 - t) + blue * t
}

