use crate::{Point3, Ray, Vec3};

pub struct Camera {
    origin: Point3,
    lower_left_corner: Point3,
    horizontal: Vec3,
    vertical: Vec3,
}

impl Camera {
    pub fn new(aspect_ratio: f64) -> Camera {
        let viewport_height = 2.0;
        let viewport_width = viewport_height * aspect_ratio;
        let focal_length = 1.0;

        let origin: Point3 = (0, 0, 0).into();
        let horizontal: Vec3 = (viewport_width, 0, 0).into();
        let vertical: Vec3 = (0, viewport_height, 0).into();
        let lower_left_corner = origin - horizontal / 2.0 - vertical / 2.0 -
            Vec3::new(0.0, 0.0, focal_length);

        Camera {
            origin,
            lower_left_corner,
            horizontal,
            vertical,
        }
    }

    pub fn get_ray(&self, x: f64, y: f64) -> Ray {
        Ray::new(self.origin, self.lower_left_corner + self.horizontal * x + self.vertical * y)
    }
}