use std::borrow::Borrow;
use std::rc::Rc;
use crate::hit::{Hit, HitRange, Hittable};
use crate::material::Material;
use crate::ray::Ray;
use crate::vec3::Point3;

pub struct Sphere {
    pub center: Point3,
    pub radius: f64,
    pub material: Rc<dyn Material>,
}

impl Hittable for Sphere {
    fn hit(&self, ray: &Ray, range: HitRange) -> Option<Hit> {
        let oc = ray.origin - self.center;
        let a = ray.direction.length().powi(2);
        let half_b = oc.dot(ray.direction);
        let c = oc.length().powi(2) - self.radius * self.radius;
        let discriminant = half_b * half_b - a * c;

        if discriminant < 0.0 {
            return None;
        }

        let sqrt = discriminant.sqrt();
        let mut root = (-half_b - sqrt) / a;

        if !range.in_range(root) {
            let root2 = (-half_b + sqrt) / a;

            if range.in_range(root2) {
                root = root2;
            } else {
                return None;
            }
        }

        let hit_point = ray.at(root);
        let outward_normal = (hit_point - self.center) / self.radius;

        Some(Hit::new(ray, root, hit_point, &outward_normal, self))
    }

    fn material(&self) -> &dyn Material {
        self.material.borrow()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn intersect() {
        let sphere = Sphere { center: (0, 0, 0).into(), radius: 1.0 };
        let ray = Ray::new((-2, 0, 0).into(), (1, 0, 0).into());

        let expected = Hit::new(&ray, 1.0, (-1, 0, 0).into(), &(-1, 0, 0).into());
        assert_eq!(sphere.hit(&ray, HitRange { min: 0., max: 99. }), Some(expected));
    }

    #[test]
    fn no_intersection() {
        let sphere = Sphere { center: (2, 2, 2).into(), radius: 1.0 };
        let ray = Ray::new((0, 2, 2).into(), (0, 1, 1).into());

        assert_eq!(sphere.hit(&ray, HitRange { min: 0., max: 99. }), None);
    }
}
