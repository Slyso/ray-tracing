use std::fs::File;
use std::io::Write;
use std::ops::Range;

use crate::colour::Colour;

pub struct ImageWriter {
    width: u32,
    height: u32,
}

pub const MAX_COLOUR_VALUE: i32 = 255;
const MAGIC_NUMBER: &str = "P3";

impl ImageWriter {
    pub fn new(width: u32, height: u32) -> ImageWriter {
        ImageWriter { width, height }
    }

    pub fn write_image<F>(&self, mut callback: F) -> std::io::Result<()>
        where F: FnMut(Range<f64>, Range<f64>) -> Colour {
        let mut file = self.create_header()?;

        for y in (0..self.height).rev() {
            for x in 0..self.width {
                let x_min = x as f64 / (self.width as f64 - 1.0);
                let x_max = (x + 1) as f64 / (self.width as f64 - 1.0);
                let y_min = y as f64 / (self.height as f64 - 1.0);
                let y_max = (y + 1) as f64 / (self.height as f64 - 1.0);

                writeln!(file, "{}", callback(x_min..x_max, y_min..y_max))?;
            }
        }

        Ok(())
    }

    fn create_header(&self) -> std::io::Result<File> {
        let mut file = File::create("target/image.ppm")?;

        let dimensions = format!("{} {}", self.width, self.height);

        writeln!(file, "{}", MAGIC_NUMBER)?;
        writeln!(file, "{}", dimensions)?;
        writeln!(file, "{}", MAX_COLOUR_VALUE)?;

        Ok(file)
    }
}