use std::fmt::{Display, Formatter};
use std::ops::{Add, AddAssign, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Neg, Range, Sub, SubAssign};

use rand::Rng;

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct Vec3 {
    values: [f64; 3],
}

pub type Point3 = Vec3;

impl Vec3 {
    pub fn new(x: f64, y: f64, z: f64) -> Vec3 {
        Vec3 { values: [x, y, z] }
    }

    pub fn new_random(x: Range<f64>, y: Range<f64>, z: Range<f64>) -> Vec3 {
        Vec3::new(
            rand::thread_rng().gen_range(x),
            rand::thread_rng().gen_range(y),
            rand::thread_rng().gen_range(z),
        )
    }

    pub fn x(self) -> f64 { self.values[0] }
    pub fn y(self) -> f64 { self.values[1] }
    pub fn z(self) -> f64 { self.values[2] }

    pub fn dot(self, other: Vec3) -> f64 {
        let multiplied = self * other;
        multiplied[0] + multiplied[1] + multiplied[2]
    }

    pub fn cross(self, other: Vec3) -> Vec3 {
        Vec3::new(
            self.y() * other.z() - self.z() * other.y(),
            self.z() * other.x() - self.x() * other.z(),
            self.x() * other.y() - self.y() * other.x(),
        )
    }

    pub fn length(self) -> f64 {
        self.dot(self).sqrt()
    }

    pub fn normalize(self) -> Vec3 {
        self / self.length()
    }

    fn apply(self, other: Vec3, operation: fn(f64, f64) -> f64) -> Vec3 {
        Vec3 {
            values: [
                operation(self[0], other[0]),
                operation(self[1], other[1]),
                operation(self[2], other[2])
            ]
        }
    }
}

impl Index<usize> for Vec3 {
    type Output = f64;

    fn index(&self, index: usize) -> &f64 {
        &self.values[index]
    }
}

impl IndexMut<usize> for Vec3 {
    fn index_mut(&mut self, index: usize) -> &mut f64 {
        &mut self.values[index]
    }
}

impl Add for Vec3 {
    type Output = Vec3;
    fn add(self, o: Self) -> Vec3 { self.apply(o, f64::add) }
}

impl AddAssign for Vec3 {
    fn add_assign(&mut self, o: Self) { *self = *self + o; }
}

impl Sub for Vec3 {
    type Output = Vec3;
    fn sub(self, o: Self) -> Vec3 { self.apply(o, f64::sub) }
}

impl SubAssign for Vec3 {
    fn sub_assign(&mut self, o: Self) { *self = *self - o; }
}

impl Mul<f64> for Vec3 {
    type Output = Vec3;
    /// Perform a scalar multiplication
    fn mul(self, scalar: f64) -> Vec3 {
        Vec3::new(self[0] * scalar, self[1] * scalar, self[2] * scalar)
    }
}

impl MulAssign<f64> for Vec3 {
    /// Perform a scalar multiplication assignment
    fn mul_assign(&mut self, scalar: f64) { *self = *self * scalar }
}

impl Mul<Vec3> for Vec3 {
    type Output = Vec3;
    /// Perform a element wise multiplication
    fn mul(self, o: Vec3) -> Vec3 {
        self.apply(o, f64::mul)
    }
}

impl MulAssign<Vec3> for Vec3 {
    /// Perform a element wise multiplication assignment
    fn mul_assign(&mut self, o: Vec3) { *self = *self * o }
}

impl Div<f64> for Vec3 {
    type Output = Vec3;
    /// Perform a scalar division
    fn div(self, scalar: f64) -> Self::Output {
        Vec3::new(self[0] / scalar, self[1] / scalar, self[2] / scalar)
    }
}

impl DivAssign<f64> for Vec3 {
    /// Perform a scalar division assignment
    fn div_assign(&mut self, scalar: f64) { *self = *self / scalar }
}

impl Neg for Vec3 {
    type Output = Vec3;

    fn neg(self) -> Self::Output {
        Vec3::new(0.0, 0.0, 0.0) - self
    }
}

impl<A, B, C> From<(A, B, C)> for Vec3
    where f64: From<A>, f64: From<B>, f64: From<C> {
    fn from(v: (A, B, C)) -> Self {
        Vec3::new(v.0.into(), v.1.into(), v.2.into())
    }
}

impl Display for Vec3 {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {}, {})", self.x(), self.y(), self.z())
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn add() {
        let a: Vec3 = (1, 2, 3).into();
        let b: Vec3 = (-4, 3, 7).into();
        assert_eq!(a + b, (-3, 5, 10).into());
    }

    #[test]
    fn add_assign() {
        let mut v: Vec3 = (0, 1, 2).into();
        v += (5, 4, 3).into();
        assert_eq!(v, (5, 5, 5).into());
    }

    #[test]
    fn sub() {
        let a: Vec3 = (1, 2, 3).into();
        let b: Vec3 = (-4, 3, 7).into();
        assert_eq!(a - b, (5, -1, -4).into());
    }

    #[test]
    fn sub_assign() {
        let mut v: Vec3 = (4, -3, 11).into();
        v -= (5, 4, 3).into();
        assert_eq!(v, (-1, -7, 8).into());
    }

    #[test]
    fn scalar_multiplication() {
        let a: Vec3 = (1, 2, 3).into();
        assert_eq!(a * 0.5, (0.5, 1.0, 1.5).into());
    }

    #[test]
    fn scalar_multiplication_assignment() {
        let mut v: Vec3 = (4, -3, 11).into();
        v *= 2.0;
        assert_eq!(v, (8, -6, 22).into());
    }

    #[test]
    fn element_wise_multiplication() {
        let a: Vec3 = (1, 2, 3).into();
        let b: Vec3 = (-4, 3, 7).into();
        assert_eq!(a * b, (-4, 6, 21).into());
    }

    #[test]
    fn element_wise_multiplication_assignment() {
        let mut v: Vec3 = (4, 3, 2).into();
        v *= Vec3::from((-1, 4, 12));
        assert_eq!(v, (-4, 12, 24).into());
    }

    #[test]
    fn scalar_division() {
        let a: Vec3 = (1, 2, 3).into();
        assert_eq!(a / 0.5, (2, 4, 6).into());
    }

    #[test]
    fn scalar_division_assignment() {
        let mut v: Vec3 = (4, -3, 11).into();
        v /= 2.0;
        assert_eq!(v, (2.0, -1.5, 5.5).into());
    }

    #[test]
    fn negation() {
        let v: Vec3 = (1, -2, 3).into();
        assert_eq!(-v, (-1, 2, -3).into());
    }

    #[test]
    fn dot() {
        let v: Vec3 = (1, 2, 3).into();
        assert_eq!(v.dot(v), 14.0);
    }

    #[test]
    fn cross() {
        let a: Vec3 = (2, 3, 4).into();
        let b: Vec3 = (5, 6, 7).into();

        assert_eq!(a.cross(b), (-3, 6, -3).into());
    }

    #[test]
    fn length() {
        let v: Vec3 = (3, -4, 0).into();
        assert_eq!(v.length(), 5.0);
    }

    #[test]
    fn normalize() {
        let v: Vec3 = (-4, 3, 0).into();
        assert_eq!(v.normalize(), (-4.0 / 5.0, 3.0 / 5.0, 0.0).into());
    }
}
