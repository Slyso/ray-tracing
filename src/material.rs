use crate::{Colour, Ray, Vec3};
use crate::hit::Hit;

pub trait Material {
    fn reflect(&self, source: &Ray, hit: &Hit) -> (Ray, Colour);
}

pub struct Diffuse {
    pub albedo: Colour,
}

impl Material for Diffuse {
    fn reflect(&self, _: &Ray, hit: &Hit) -> (Ray, Colour) {
        let reflected_ray = hit.normal() + random_vector_in_unit_sphere();
        (Ray::new(hit.point(), reflected_ray), self.albedo)
    }
}

pub struct Metal {
    pub albedo: Colour,
}

impl Material for Metal {
    fn reflect(&self, source: &Ray, hit: &Hit) -> (Ray, Colour) {
        let reflected_ray = reflect(source.direction, hit.normal());
        (Ray::new(hit.point(), reflected_ray), self.albedo)
    }
}

fn reflect(v: Vec3, n: Vec3) -> Vec3 {
    v - n * 2.0 * v.dot(n)
}

fn random_vector_in_unit_sphere() -> Vec3 {
    loop {
        let vector = Vec3::new_random(-1.0..1.0, -1.0..1.0, -1.0..1.0);
        if vector.length() < 1.0 {
            return vector;
        }
    }
}

